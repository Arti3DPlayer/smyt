# -*- coding: utf-8 -*-
from .models import dynamic_models
from django.utils import simplejson as json
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.http import Http404, HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import datetime

def index(request):
    return render_to_response('smyt/index.html')


def model_list(request):
    data = [dict(name= model.__name__.lower(), title= model._meta.verbose_name) for model in dynamic_models.registry.values()]
    return HttpResponse(json.dumps(dict(data= data)), mimetype= 'application/json')

def model_data(request, name):
    _name = name
    model = dynamic_models.registry.get(name)
    if not model:
        raise Http404()
    title = model._meta.verbose_name
    values =list()
    for kw in model.objects.all().values(*[field.name for field in model._meta.fields]):
        l = list()
        for name in [field.name for field in model._meta.fields]: 
            l.append(dict(
                name= name, 
                id=kw.get('id'), 
                value= kw.get(name) if not isinstance(kw.get(name), datetime.date) else kw.get(name).strftime('%d.%m.%Y')))
        values.append(dict(fields= l))
    fields = [dict(name= field.name, title= field.verbose_name, type= getattr(field, '_type', None))  for field in model._meta.fields]
    out = dict(title= title, values= list(values), fields= fields, name= _name)
    return HttpResponse(json.dumps(out, cls= DjangoJSONEncoder), mimetype= 'application/json')

def model_add(request, name):
    if not request.is_ajax():
      raise Http404()  
    model = dynamic_models.registry.get(name)
    form = model.Form(data= request.GET or None)
    if form.is_valid():
        form.save()
    return HttpResponse()
    
def model_edit(request, name):
    if not request.is_ajax():
      raise Http404()  
    model = dynamic_models.registry.get(name)
    try:
        obj = get_object_or_404(model, id= request.GET.get('id', 0))
    except ValueError:
        raise Http404()
    data = dict()
    for name in [field.name for field in model._meta.fields]:
        data[name] = request.GET.get(name) or getattr(obj, name)
    form = model.Form(data= data, instance= obj)
    if form.is_valid():
        form.save()
        return HttpResponse()    
    raise Http404()
    